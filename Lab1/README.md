-------------
ECE 281 Lab 1
-------------
By C3C Allison Wong

Preliminary Design
==================

Truth Table
-----------
![Truth table.JPG](images/Truth table.JPG "")

Question
--------

Q: For each output, if you had to implement the full SOP or POS equations in
hardware, which one would you choose and why?

A: Sum of Products would be easier to implement. Summing multiple versions of ABCDEFG is much more efficient than finding the product of multiple versions of (A+B+C+D+E+F+G).

Boolean Equations
-----------------
![Boolean Equations.JPG](images/Boolean Equations.JPG "")

K Maps
------
![KMap A B.JPG](images/KMap A B.JPG "")
![KMap C.JPG](images/KMap C.JPG "")
![KMap D E.JPG](images/KMap D E.JPG "")
![KMap F.JPG](images/KMap F.JPG "")
![KMap G.JPG](images/KMap G.JPG "")

Results
=======

Behavioral Simulation Waveform
------------------------------
![Behavioral Simulation Waveform.JPG](images/Behavioral Simulation Waveform.JPG "")

Hardware Demo
-------------
I demoed the hardware for Capt Johnson in class on T10.

The results were as expected, the seven segment display displaying all the expected combinations, according to the truth table, when the switches were flipped and the button was pressed.

Feedback
========
Number of hours spent on Lab 1: 3 hours.
Suggestions: More explanation in class about the test plan process (rather than relying on knowing it worked in CE2 and just copying it), and the purpose of the top level.

Documentation Statement
=======================
C1C Hanson helped me understand the test plan process of the test bench, and the purpose of the top level.
