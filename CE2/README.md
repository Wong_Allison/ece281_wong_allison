----
ECE 281 CE2 - Half-Adder in VDHL
----

By C3C Allison Wong

Objectives
==========

-   Implement and test a simple logic design (half-adder) using VDHL

-   Gain more experience using tools (Git, Markdown, Xilinx Vivado)

Preliminary Design
==================

![](media/fe343fd8e5a515ba6accb5860a3a2f54.jpg)

The following is how the adder will be hooked up to the board:

-   A-\> sw1

-   B-\> sw0

-   S-\> led0

-   Cout-\> led1

![](media/a600f74d7d8982bc890e5783a1a4210b.jpg)

Results
=======

Half-Adder Wave Configuration:
------------------------------

![](media/5d31cb27181aac44fd1d8a4812a30c26.jpg)

The simulation results did match the expected results.

Half-Adder Schematic:
---------------------

![](media/9c93d339e6a1d65feb285499d37a902a.jpg)

The schematic matches the expected half-adder schematic.

Half-Adder Synthesis Schematic:
-------------------------------

![](media/53a8731282063dc60994764d03f0bae7.jpg)

Hardware Demo Video
-------------------

<https://www.youtube.com/watch?v=6AdQzF7Be3k&feature=youtu.be>

The hardware worked successfully as expected.

The project was a success.

Feedback
========

Number of hours spent on CE2: 2 hours

CE2 did a great job at showing how the changes in each bit of code impacted the
result. The step-by-step tutorial was helpful.

Documentation Statement
=======================

I asked C3C Tricia Dang if she had encountered a similar bitstream error that I
did. She had not.
