# $a0 = input, $v0 = output
	add $v0, $0, $0
loop:   beq $a0, $0, done
	andi $t0, $a0, 0x1
	beq $t0, $0, shift
	addi $v0, $v0, 1
shift:  srl $a0, $a0, 1
	j loop		# this label name could be more helpful!
done:
