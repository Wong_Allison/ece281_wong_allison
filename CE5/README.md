----
ECE 281 CE5 - MIPS Programs
----

By C3C Allison Wong

Analysis
========

Program 1 (CE5_Mystery1.asm)
----------------------------
What does it do? ***The program initializes $t0, $t1, and $t2, to 5, 20, and 0, respectively, and then adds $t2 to $t1 and adds 4 to $t2 repeatedly until $t2=$t1.***

How many times does the loop run? ***5 times***

What is the final value of the register $t0? ***0x2d, or 45***

Program 2 (CE5_Mystery2.asm)
----------------------------
What does it do? 

***The program loops, repeatedly dividing $a0 by 2, until it reaches 0, when the program then ends.***

Provide the machine code for all branch and jump instructions. 

***10800005, 11000001, 08100001.***

Which instructions changed? 

***Only the jump instruction (j loop) changed. The others (branch) did not change because they are relative to the (changing) pc register, while the address for the jump is specified in the j instruction.***

What segment of the MIPS memory map is the assmebly code? How much memory is required for the assembly code for that segment?

***Text. Since each line is 32 bits, and there are 7 lines, the 7 lines of assembly code is 224 bits. The entire text segment is approximately 33 MB.***


Program 3 (CE5_Mystery3.asm)
----------------------------
What does it do? 

***The program checks if the addition of $t2 and $t3 results in overflow.***

MIPS Assembly Snippets
======================

1. Store the 2's complement of $s0 into $s1 (2 instructions)

    ***nor $s1, $s0, $0***
    
    ***addi $s1, $s1, 1***    

2. Divides $s0 by 8 and stores the result into $s1. (Should work with negative inputs)

    ***sra $s1, $s0, 3***

3. Stores 0 in $s1 if the contents of $s0 are even. Otherwise, a 1 is stored in $s1 (1 instruction)

    ***andi $s1, $s0, 1***
    