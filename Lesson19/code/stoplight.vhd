--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : stoplight.vhd
--| AUTHOR(S)     : C3C Allison Wong
--| CREATED       : 02/28/2018
--| DESCRIPTION   : This module file implements the HW stoplight example using 
--|				  : direct hardware mapping (registers and CL) for BINARY encoding.
--|               : Reset is asynchronous with a default state of yellow.
--|
--| DOCUMENTATION : NONE
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std
--|    Files     : None
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

  
entity stoplight is
    Port ( C     : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           clk   : in  STD_LOGIC;
           R     : out  STD_LOGIC;
           Y     : out  STD_LOGIC;
           G     : out  STD_LOGIC);
end stoplight;

architecture stoplight_arch of stoplight is 
	
	-- create register signals with default state yellow
    signal Q_next, Q: std_logic_vector(1 downto 0) := "10";
  
begin
	-- Next state logic ---------------------------------
	Q_next(1) <= not Q(1) and Q(0) and not C;
	Q_next(0) <= not Q(1) and C;
	
	-- Output logic     ---------------------------------
	R <= Q(1) xnor Q(0);
	Y <= Q(1) and not Q(0);
	G <= not Q(1) and Q(0);
	
	-- PROCESSES ----------------------------------------
	
	-- state memory w/ asyncrhonous reset ---------------
	register_proc : process (clk,reset )
	begin
	   if reset = '1' then
	       Q <= "10";--Reset state is yellow
       elsif (rising_edge(clk)) then
           Q <= Q_next;
       end if;
	end process register_proc;
	-------------------------------------------------------
	
end stoplight_arch;
