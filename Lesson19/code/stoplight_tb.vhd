----------------------------------------------------------------------------------
-- Name:	C3C Allison Wong
-- Date:	Spring 2017
-- Course: ECE 281
-- File: stoplight_tb.vhd
-- Purp:	Testbench for Basic Stoplight State Machine
-- Doc:	None
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY stoplight_tb IS
END stoplight_tb;
 
ARCHITECTURE behavior OF stoplight_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT stoplight
    PORT(
         C : IN  std_logic;
         reset : IN  std_logic;
         clk : IN  std_logic;
         R : OUT  std_logic;
         Y : OUT  std_logic;
         G : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal C : std_logic := '0';
   signal reset : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal R : std_logic;
   signal Y : std_logic;
   signal G : std_logic;

   -- Clock period definitions
   constant k_clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: stoplight PORT MAP (
          C => C,
          reset => reset,
          clk => clk,
          R => R,
          Y => Y,
          G => G
        );

   -- Clock process definitions
   clk_proc : process
   begin
		clk <= '0';
		wait for k_clk_period/2;
		clk <= '1';
		wait for k_clk_period/2;
   end process;
  
  
   -- Stimulus process
   -- Use 220 ns for simulation
   stim_proc: process
   begin
        -- sequential timing		
		reset <= '1';
		wait for k_clk_period*1;
		
		reset <= '0';
		wait for k_clk_period*1;
		
		-- alternative way of implementing Finite State Machine Inputs
		-- starts after "wait for" statements
		-- statements after this one start in paralell to this one
		C <= '0', '1' after 40 ns, '0' after 80ns, '1' after 120 ns, '0' after 160 ns, '1' after 170 ns;

        -- one way to make using the reset easier would be to use a separate process to control it
        wait for k_clk_period*19;
        reset <= '1';

      wait;
   end process;

END;
