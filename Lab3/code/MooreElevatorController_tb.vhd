--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : MooreElevatorController_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : C3C Allison Wong
--| CREATED       : 03/13/18
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : None.
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity MooreElevatorController_tb is
end MooreElevatorController_tb;

architecture test_bench of MooreElevatorController_tb is 
	
  -- declare the component of your top-level design unit under test (UUT)
  component MooreElevatorController is
    port(      clk     : in  STD_LOGIC;
               reset   : in  STD_LOGIC;
               stop    : in  STD_LOGIC;
               up_down : in  STD_LOGIC;
               floor   : out STD_LOGIC_VECTOR (3 downto 0)
    );	
  end component;


  signal c_clk : std_logic := '0';
  signal c_reset : std_logic := '0';
  signal c_stop : std_logic := '0';
  signal c_up_down : std_logic := '0';
  signal c_floor : std_logic_vector (3 downto 0);
  
  constant clk_per : time := 20 ns;
  
begin
	
	uut : MooreElevatorController port map(
	       clk => c_clk,
	       reset => c_reset,
	       stop => c_stop,
	       up_down => c_up_down,
	       floor => c_floor
	);
	
	clk : process
	begin
	       c_clk <= '0';
	           wait for clk_per/2;
	       c_clk <= '1';
	           wait for clk_per/2;
	end process;
	
	-- Test Plan Process --------------------------------
	-- Implement the test plan here.  Body of process is continuously from time = 0  
	test_process : process 
	begin
	
		-- start with a reset for a while
		c_reset <= '1';
		      wait for clk_per;
		
		-- start stopped at floor 1 for 2 clock cycles
		c_up_down <= '1'; c_stop <= '1'; c_reset <= '0';      
		       wait for clk_per*2;
		        
		-- go up to floor 4, stopping for 2 clock cycles at each floor
	    -- floor 2, wait for 2 cycles
	    c_stop <= '0';
	           wait for clk_per;
	    c_stop <= '1';
	           wait for clk_per*2;
	           	          	    
	    -- floor 3, wait for 2 cycles
	    c_stop <= '0';
               wait for clk_per;
        c_stop <= '1';
               wait for clk_per*2;
	    
        -- stops at 4th floor without using stop
        c_up_down <= '1'; c_stop <= '0';
               wait for clk_per*2;
               
        -- return down to floor 1 without stopping and stop at floor 1 wihtout using stop
        c_up_down <= '0'; c_stop <= '0';
    		
		wait; -- wait forever
	end process;	
	-----------------------------------------------------	
	
end test_bench;
