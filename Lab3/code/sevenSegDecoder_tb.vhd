--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : sevenSegDecoder_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : C3C Allison Wong
--| CREATED       : 29 January 2018
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : Refer to README
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity sevenSegDecoder_tb is
end sevenSegDecoder_tb;

architecture test_bench of sevenSegDecoder_tb is 
	
  -- declare the component of your top-level design unit under test (UUT)
  component sevenSegDecoder is
    port(
      i_D  : in  std_logic_vector(3 downto 0); 
      o_S  : out std_logic_vector(6 downto 0) 
    );
  end component;

  -- declare any additional components required
  
  -- declare signals needed to stimulate the UUT inputs
  signal i_D : std_logic_vector(3 downto 0);
  signal i_sw0 : std_logic;
  signal i_sw1 : std_logic;
  signal i_sw2 : std_logic;
  signal i_sw3 : std_logic;
  -- also need signals for the outputs of the UUT
  signal c_Sa : std_logic := '1';
  signal c_Sb : std_logic := '1';
  signal c_Sc : std_logic := '1';
  signal c_Sd : std_logic := '1';
  signal c_Se : std_logic := '1';
  signal c_Sf : std_logic := '1';
  signal c_Sg : std_logic := '1';	
  
begin
	-- PORT MAPS ----------------------------------------

	-- map ports for any component instances (port mapping is like wiring hardware)
	uut_inst : sevenSegDecoder port map (
		i_D(3) => i_sw3,  -- notice comma (not a semicolon)
		i_D(2) => i_sw2,
		i_D(1) => i_sw1,
		i_D(0) => i_sw0,
		o_S(6) => c_Sa,
        o_S(5) => c_Sb,
        o_S(4) => c_Sc,
        o_S(3) => c_Sd,
        o_S(2) => c_Se,
        o_S(1) => c_Sf,
        o_S(0) => c_Sg  -- notice no comma
	);

	-- Test Plan Process --------------------------------
	-- Implement the test plan here.  Body of process is continuously from time = 0  
	test_process : process 
	begin
		-- assign '0' for first 100 ns, then '1' for next 100 ns, then '0'
		i_sw3 <= '0'; i_sw2 <= '0'; i_sw1 <= '0'; i_sw0 <= '0';
                wait for 10 ns;     
		i_sw3 <= '0'; i_sw2 <= '0'; i_sw1 <= '0'; i_sw0 <= '1';
                wait for 10 ns; 	
		i_sw3 <= '0'; i_sw2 <= '0'; i_sw1 <= '1'; i_sw0 <= '0';
                wait for 10 ns;   
		i_sw3 <= '0'; i_sw2 <= '0'; i_sw1 <= '1'; i_sw0 <= '1';
                wait for 10 ns;
		i_sw3 <= '0'; i_sw2 <= '1'; i_sw1 <= '0'; i_sw0 <= '0';
                wait for 10 ns;     
        i_sw3 <= '0'; i_sw2 <= '1'; i_sw1 <= '0'; i_sw0 <= '1';
                wait for 10 ns;     
        i_sw3 <= '0'; i_sw2 <= '1'; i_sw1 <= '1'; i_sw0 <= '0';
                wait for 10 ns;   
        i_sw3 <= '0'; i_sw2 <= '1'; i_sw1 <= '1'; i_sw0 <= '1';
                wait for 10 ns;  
		i_sw3 <= '1'; i_sw2 <= '0'; i_sw1 <= '0'; i_sw0 <= '0';
                wait for 10 ns;     
        i_sw3 <= '1'; i_sw2 <= '0'; i_sw1 <= '0'; i_sw0 <= '1';
                wait for 10 ns;     
        i_sw3 <= '1'; i_sw2 <= '0'; i_sw1 <= '1'; i_sw0 <= '0';
                wait for 10 ns;   
        i_sw3 <= '1'; i_sw2 <= '0'; i_sw1 <= '1'; i_sw0 <= '1';
                wait for 10 ns;
        i_sw3 <= '1'; i_sw2 <= '1'; i_sw1 <= '0'; i_sw0 <= '0';
                wait for 10 ns;     
        i_sw3 <= '1'; i_sw2 <= '1'; i_sw1 <= '0'; i_sw0 <= '1';
                wait for 10 ns;     
        i_sw3 <= '1'; i_sw2 <= '1'; i_sw1 <= '1'; i_sw0 <= '0';
                wait for 10 ns;   
        i_sw3 <= '1'; i_sw2 <= '1'; i_sw1 <= '1'; i_sw0 <= '1';
                wait for 10 ns;                                        
                                            
		wait; -- wait forever
	end process;	
	-----------------------------------------------------	
	
end test_bench;
