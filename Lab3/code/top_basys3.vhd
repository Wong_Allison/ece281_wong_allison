--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : top_basys3.vhd
--| AUTHOR(S)     : C3C Allison Wong
--| CREATED       : 3/21/2018
--| DESCRIPTION   : This file implements the top level module for a BASYS 3 to 
--|					drive the Lab 3 Design Project (Advanced Elevator Controller).
--|
--|					Inputs: clk       --> 100 MHz clock from FPGA
--|							btnL      --> Rst Clk
--|							btnR      --> Rst FSM
--|							btnU      --> Rst Master
--|							btnC      --> GO (request floor)
--|							sw(15:12) --> Passenger location (floor select bits)
--| 						sw(3:0)   --> Desired location (floor select bits)
--| 						 - REQUIRED FUNCTIONALITY ONLY: sw(1) --> up_down, sw(0) --> stop
--|							 
--|					Outputs: led --> indicates elevator movement with sweeping pattern
--|							   - led(10) --> led(15) = MOVING UP
--|							   - led(5)  --> led(0)  = MOVING DOWN
--|							   - ALL OFF		     = NOT MOVING
--|							 an(3:0)    --> seven-segment display anode active-low enable (AN3 ... AN0)
--|							 seg(6:0)	--> seven-segment display cathodes (CG ... CA.  DP unused)
--|
--| DOCUMENTATION : None
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std
--|    Files     : MooreElevatorController.vhd, clock_divider.vhd, sevenSegDecoder.vhd
--|				   thunderbird_fsm.vhd, TDM4.vhd, OTHERS???
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;


entity top_basys3 is
	port(

		clk     :   in std_logic; -- native 100MHz FPGA clock
		
		-- Switches (16 total)
		sw  	:   in std_logic_vector(15 downto 0);
		
		-- Buttons (5 total)
		btnC	:	in	std_logic;					  -- GO
		btnU	:	in	std_logic;					  -- master_reset
		btnL	:	in	std_logic;                    -- clk_reset
		btnR	:	in	std_logic;	                  -- fsm_reset
		--btnD	:	in	std_logic;			
		
		-- LEDs (16 total)
		led 	:   out std_logic_vector(15 downto 0);

		-- 7-segment display segments (active-low cathodes CG ... CA)
		seg		:	out std_logic_vector(6 downto 0);  -- seg(6) = CG, seg(0) = CA

		-- 7-segment display active-low enables (anodes)
		an      :	out std_logic_vector(3 downto 0)
	);
end top_basys3;

architecture top_basys3_arch of top_basys3 is 
  
	-- declare components and signals
	component MooreElevatorController is
        Port ( clk_fsm : in  STD_LOGIC;
               reset   : in  STD_LOGIC;
               go      : in  STD_LOGIC;
               i_start : in  STD_LOGIC_VECTOR (3 downto 0);
               i_whereto : in  STD_LOGIC_VECTOR (3 downto 0);
               o_floor : out STD_LOGIC_VECTOR (3 downto 0);
               o_left  : out STD_LOGIC;
               o_right : out STD_LOGIC
             );
    end component;
    
    component clock_divider is
        generic ( constant k_DIV : natural := 25000000    ); -- How many clk cycles until slow clock toggles 
                                                   -- Effectively, you divide the clk double this 
                                                   -- number (e.g., k_DIV := 2 --> clock divider of 4)
        port (  i_clk    : in std_logic;
                i_reset  : in std_logic;           -- asynchronous
                o_clk    : out std_logic           -- divided (slow) clock
        );
    end component;
    
    component sevenSegDecoder is 
      port(
        i_D  : in  std_logic_vector(3 downto 0); -- 1-bit input port
        o_S  : out std_logic_vector(6 downto 0)  -- 1-bit output port (NOTE: NO semicolon here!)
      );
    end component;
    
    component TDM4 is
        generic ( constant k_WIDTH : natural  := 4); -- bits in input and output
        Port ( i_CLK         : in  STD_LOGIC;
               i_RESET         : in  STD_LOGIC; -- asynchronous
               i_D3         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               i_D2         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               i_D1         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               i_D0         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               o_DATA        : out STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               o_SEL        : out STD_LOGIC_VECTOR (3 downto 0)    -- selected data line (one-cold)
        );
    end component;
    
    component thunderbird_fsm is 
      port(
        i_left, i_right  : in std_logic; 
        i_clk, i_reset   : in std_logic;
        o_lights_R  : out std_logic_vector(5 downto 0);  -- a = 0, b = 1, c = 2
        o_lights_L  : out std_logic_vector(5 downto 0) -- a = 0, b = 1, c = 2
      );
    end component;
    
    signal c_clk : std_logic; -- from clock divider to MooreElevatorController (slow)
    signal c_clk_two : std_logic; -- from clock divider to TDM (fast)
    signal c_clk_three : std_logic; -- from clock divider to thunderbird (medium)
    signal clk_reset : std_logic; -- from OR gate to clock divider
    signal fsm_reset : std_logic; -- from other OR gate to MooreElevatorController
    signal c_floor_TDM : std_logic_vector(3 downto 0); -- from ElevatorController to TDM4
    signal c_floor_Seg : std_logic_vector(3 downto 0); -- from TDM4 to sevenSegDecoder
    signal c_floortens : std_logic_vector(3 downto 0); -- tens digit to sevenSegDecoder
    signal c_floorones : std_logic_vector(3 downto 0); -- ones digit to sevenSegDecoder
    signal c_elev_up   : std_logic := '0'; -- '1' if elev_dir = '1', goes to thunderbird_fsm
    signal c_elev_down : std_logic := '0'; -- '1' if elev_dir = '0', goes to thunderbird_fsm
 
begin
	-- PORT MAPS ----------------------------------------
    clk_div : clock_divider -- connected to MooreElevatorController
    generic map (k_DIV => 25000000)
    port map(
            i_clk => clk,
            i_reset => clk_reset,
            o_clk => c_clk
    );
    
    clk_div_two : clock_divider -- connected to TDM to sevenSegDecoder
    generic map (k_DIV => 200000) 
    port map(
            i_clk => clk,
            i_reset => btnU,
            o_clk => c_clk_two
    );
    
    clk_div_three : clock_divider -- connected to thunderbird_fsm
    generic map (k_DIV => 6250000)
    port map(
            i_clk => clk,
            i_reset => btnU,
            o_clk => c_clk_three
    );
    
    elevate : MooreElevatorController port map(
            clk_fsm => c_clk,
            reset => fsm_reset,
            go => btnC,
            i_start => sw(15 downto 12),
            i_whereto => sw(3 downto 0),
            o_floor => c_floor_TDM,
            o_left => c_elev_up,
            o_right => c_elev_down
    );
    
    tdm_4 : TDM4 port map(
        i_CLK =>  c_clk_two,
        i_RESET => btnU,
        i_D3 => c_floortens,
        i_D2 => c_floorones,
        i_D1 => "0000",
        i_D0 => "0000",
        o_DATA => c_floor_Seg,
        o_SEL => an
    );
        
    segments : sevenSegDecoder port map(
            i_D => c_floor_Seg,
            o_S(0) => seg(6),
            o_S(1) => seg(5),
            o_S(2) => seg(4),
            o_S(3) => seg(3),
            o_S(4) => seg(2),
            o_S(5) => seg(1),
            o_S(6) => seg(0)
    );

    bird : thunderbird_fsm port map( 
            i_left => c_elev_up, 
            i_right => c_elev_down,
            i_clk => c_clk_three,
            i_reset => btnR,
            o_lights_R => led(5 downto 0),
            o_lights_L => led(15 downto 10)
    );
	
	-- CONCURRENT STATEMENTS ----------------------------
	
	clk_reset <= btnU or btnL;
	fsm_reset <= btnU or btnR;
	c_floortens <= "0001" when (c_floor_TDM(3) = '1' and c_floor_TDM(1) = '1') or
	                         (c_floor_TDM(3) = '1' and c_floor_TDM(2) = '1') else "0000";
	c_floorones <= "0000" when c_floor_TDM = "1010" else
	               "0001" when c_floor_TDM = "1011" else
	               "0010" when c_floor_TDM = "1100" else
	               "0011" when c_floor_TDM = "1101" else
	               "0100" when c_floor_TDM = "1110" else
	               "0101" when c_floor_TDM = "1111" else
	               c_floor_TDM;                 
	
	-- ground unused LEDs (which is all of them for REQUIRED functionality)
	led(9 downto 6) <= (others => '0');

	-- leave unused switches UNCONNECTED
	
	-- Ignore the warnings associated with these signals
	-- Alternatively, you can create a different board implementation, 
	--   or make additional adjustments to the constraints file
	
    an(1) <= '1';
    an(0) <= '1';
	-- wire up active-low 7SD anodes (an) as required
	-- Tie any unused anodes to power ('1') to keep them off
	
end top_basys3_arch;
