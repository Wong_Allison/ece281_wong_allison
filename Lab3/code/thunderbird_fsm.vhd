--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : thunderbird_fsm.vhd
--| AUTHOR(S)     : C3C Allison Wong
--| CREATED       : 03/21/2017
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : None.
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;

-- entity thunderbird_fsm
entity thunderbird_fsm is 
  port(
	i_left, i_right  : in std_logic; 
	i_clk, i_reset   : in std_logic;
	o_lights_R  : out std_logic_vector(5 downto 0);  -- a = 0, b = 1, c = 2
	o_lights_L  : out std_logic_vector(5 downto 0) -- a = 0, b = 1, c = 2
  );
end thunderbird_fsm;

architecture thunderbird_fsm_arch of thunderbird_fsm is 

	type sm_thunderbird is(s_reset, s_L1, s_L2, s_L3, s_R1, s_R2, s_R3, s_haz);	
	signal Q_next, Q : std_logic_vector(2 downto 0) := "000";
  
begin

    -- PROCESSES ----------------------------------------	
	--next state process and logic
	Q_next_process : process (Q, i_left, i_right)
	begin
	          
	          
	           Q_next(2) <= (not Q(2) and not Q(1) and not Q(0) and i_left) or (Q(2) and not Q(1));
	           Q_next(1) <= (not Q(2) and not Q(1) and not Q(0) and i_left and i_right) or (not Q(2) and Q(1) and not Q(0)) or (not Q(1) and Q(0));
	           Q_next(0) <= (not Q(2) and not Q(1) and not Q(0) and i_right) or (not Q(2) and Q(1) and not Q(0)) or (Q(2) and not Q(1) and not Q(0));

                       
end process;

--Clock process, if on rising edge, switch states
clk_process : process(i_clk, i_reset)
begin
        if i_reset = '1' then
                Q <= "000"; -- instead of s_reset
        elsif (rising_edge(i_clk)) then
                Q <= Q_next;
    end if;
end process;

--Output process
out_process : process(Q)
begin
                o_lights_L(5) <= Q(2) and Q(1);
                o_lights_L(4) <= Q(2) and Q(1);
                o_lights_L(3) <= (Q(2) and not Q(1) and Q(0)) or (Q(2) and Q(1));
                o_lights_L(2) <= (Q(2) and not Q(1) and Q(0)) or (Q(2) and Q(1));
                o_lights_L(1) <= Q(2);
                o_lights_L(0) <= Q(2);

                o_lights_R(5) <= (not Q(2) and Q(1)) or (not Q(2) and not Q(1) and Q(0)) or (Q(2) and Q(1) and Q(0));
                o_lights_R(4) <= (not Q(2) and Q(1)) or (not Q(2) and not Q(1) and Q(0)) or (Q(2) and Q(1) and Q(0));
                o_lights_R(3) <= (not Q(2) and Q(1)) or (Q(2) and Q(1) and Q(0));
                o_lights_R(2) <= (not Q(2) and Q(1)) or (Q(2) and Q(1) and Q(0));
                o_lights_R(1) <= Q(1) and Q(0);
                o_lights_R(0) <= Q(1) and Q(0);
end process;             
                        	        
end thunderbird_fsm_arch;
