--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : MooreElevatorController.vhd
--| AUTHOR(S)     : C3C Allison Wong
--| CREATED       : 03/21/2018
--| DESCRIPTION   : This file implements the CE4/Lab3 elevator controller (Moore Machine)
--|
--|  The system is specified as follows:
--|   - The elevator controller will traverse four floors (numbered 1 to 4).
--|   - It has two external inputs, Up_Down and Stop.
--|   - When Up_Down is active and Stop is inactive, the elevator will move up 
--|			until it reaches the top floor (one floor per clock, of course).
--|   - When Up_Down is inactive and Stop is inactive, the elevator will move down 
--|			until it reaches the bottom floor (one floor per clock).
--|   - When Stop is active, the system stops at the current floor.  
--|   - When the elevator is at the top floor, it will stay there until Up_Down 
--|			goes inactive while Stop is inactive.  Likewise, it will remain at the bottom 
--|			until told to go up and Stop is inactive.  
--|   - The system should output the floor it is on (1 – 4) as a four-bit binary number.
--|  
--| DOCUMENTATION : None.
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : None
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MooreElevatorController is
    Port ( clk_fsm     : in  STD_LOGIC;
           reset   : in  STD_LOGIC; 
           go      : in  STD_LOGIC;
           i_start : in  STD_LOGIC_VECTOR (3 downto 0);
           i_whereto : in  STD_LOGIC_VECTOR (3 downto 0);
           o_floor : out STD_LOGIC_VECTOR (3 downto 0);
           o_left  : out STD_LOGIC;
           o_right : out STD_LOGIC
		 );
end MooreElevatorController;

-- Write the code in a similar style as the Lesson 19 ICE (stoplight FSM version 2)
architecture Behavioral of MooreElevatorController is

    -- Below you create a new variable type! You also define what values that 
    -- variable type can take on. Now you can assign a signal as 
    -- "sm_floor" the same way you'd assign a signal as std_logic
	type sm_floor is (s_floor1, s_floor2, s_floor3, s_floor4, s_floor5, s_floor6, s_floor7, s_floor8, s_floor9, s_floor10, s_floor11, s_floor12, s_floor13, s_floor14, s_floor15);
	
	-- Here you create variables that can take on the values
	-- defined above. Neat-o! super duper neat-o!
	signal current_state : sm_floor;

	signal start_floor : std_logic_vector (3 downto 0);
	signal whereto_floor : std_logic_vector (3 downto 0);
	signal go_to : std_logic_vector (3 downto 0);
	signal up   : std_logic;
	signal down : std_logic;

begin
	fsm : process (clk_fsm, go)
	begin
	   if reset = '1' then
	       current_state <= s_floor1;
	       
	   elsif go = '1' then
	       start_floor <= i_start;
	       whereto_floor <= i_whereto;
	       go_to <= start_floor;
	       
	   elsif rising_edge(clk_fsm) then
	       if go_to = "0000" then
	           current_state <= s_floor1;
	           up <= '0';
	           down <= '0';
	       end if;   
	   
	       case current_state is
	           when s_floor1 =>
	               if go_to > "0001" then
	                   current_state <= s_floor2;
	                   up <= '1';
	                   down <= '0';
	               else
	                   go_to <= whereto_floor;
	               end if;
	           
	           when s_floor2 =>
	               if go_to > "0010" then
	                   current_state <= s_floor3;
	                   up <= '1';
	                   down <= '0';
	               elsif go_to < "0010" then
	                   current_state <= s_floor1;
	                   up <= '0';
	                   down <= '1';
	               else
	                   go_to <= whereto_floor;
	                   up <= '0';
	                   down <= '0';
	               end if; 
	           
	           when s_floor3 =>
                   if go_to > "0011" then
                       current_state <= s_floor4;
                       up <= '1';
                       down <= '0';
                   elsif go_to < "0011" then
                       current_state <= s_floor2;
                       up <= '0';
                       down <= '1';
                   else
                       go_to <= whereto_floor;
                       up <= '0';
                       down <= '0';
                   end if;  
	            
	           when s_floor4 =>
                   if go_to > "0100" then
                       current_state <= s_floor5;
                       up <= '1';
                       down <= '0';
                   elsif go_to < "0100" then
                       current_state <= s_floor3;
                       up <= '0';
                       down <= '1';
                   else
                       go_to <= whereto_floor;
                       up <= '0';
                       down <= '0';
                   end if;
                   
               when s_floor5 =>
                   if go_to > "0101" then
                       current_state <= s_floor6;
                       up <= '1';
                       down <= '0';
                   elsif go_to < "0101" then
                       current_state <= s_floor4;
                       up <= '0';
                       down <= '1';
                   else
                       go_to <= whereto_floor;
                       up <= '0';
                       down <= '0';
                   end if;
               
	           when s_floor6 =>
                   if go_to > "0110" then
                       current_state <= s_floor7;
                       up <= '1';
                       down <= '0';
                   elsif go_to < "0110" then
                       current_state <= s_floor5;
                       up <= '0';
                       down <= '1';
                   else
                       go_to <= whereto_floor;
                       up <= '0';
                       down <= '0';
                   end if;
                   
               when s_floor7 =>
                   if go_to > "0111" then
                       current_state <= s_floor8;
                       up <= '1';
                       down <= '0';
                   elsif go_to < "0111" then
                       current_state <= s_floor6;
                       up <= '0';
                       down <= '1';
                   else
                       go_to <= whereto_floor;
                       up <= '0';
                       down <= '0';
                   end if;
              
	          when s_floor8 =>
                  if go_to > "1000" then
                      current_state <= s_floor9;
                      up <= '1';
                      down <= '0';
                  elsif go_to < "1000" then
                      current_state <= s_floor7;
                      up <= '0';
                      down <= '1';
                  else
                      go_to <= whereto_floor;
                      up <= '0';
                      down <= '0';
                  end if;
                 
              when s_floor9 =>
                  if go_to > "1001" then
                      current_state <= s_floor10;
                      up <= '1';
                      down <= '0';
                  elsif go_to < "1001" then
                      current_state <= s_floor8;
                      up <= '0';
                      down <= '1';
                  else
                      go_to <= whereto_floor;
                      up <= '0';
                      down <= '0';
                  end if;
              
              when s_floor10 =>
                  if go_to > "1010" then
                      current_state <= s_floor11;
                      up <= '1';
                      down <= '0';
                  elsif go_to < "1010" then
                      current_state <= s_floor9;
                      up <= '0';
                      down <= '1';
                  else
                      go_to <= whereto_floor;
                      up <= '0';
                      down <= '0';
                  end if;
                  
              when s_floor11 =>
                  if go_to > "1011" then
                      current_state <= s_floor12;
                      up <= '1';
                      down <= '0';
                  elsif go_to < "1011" then
                      current_state <= s_floor10;
                      up <= '0';
                      down <= '1';
                  else
                      go_to <= whereto_floor;
                      up <= '0';
                      down <= '0';
                  end if;     

              when s_floor12 =>
                  if go_to > "1100" then
                      current_state <= s_floor13;
                      up <= '1';
                      down <= '0';
                  elsif go_to < "1100" then
                      current_state <= s_floor11;
                      up <= '0';
                      down <= '1';
                  else
                      go_to <= whereto_floor;
                      up <= '0';
                      down <= '0';
                  end if;
                  
              when s_floor13 =>
                  if go_to > "1101" then
                      current_state <= s_floor14;
                      up <= '1';
                      down <= '0';
                  elsif go_to < "1101" then
                      current_state <= s_floor12;
                      up <= '0';
                      down <= '1';
                  else
                      go_to <= whereto_floor;
                      up <= '0';
                      down <= '0';
                  end if;     

              when s_floor14 =>
                  if go_to > "1110" then
                      current_state <= s_floor15;
                      up <= '1';
                      down <= '0';
                  elsif go_to < "1110" then
                      current_state <= s_floor13;
                      up <= '0';
                      down <= '1';
                  else
                      go_to <= whereto_floor;
                      up <= '0';
                      down <= '0';
                  end if;
                  
              when s_floor15 =>
                  if go_to < "1111" then
                      current_state <= s_floor14;
                      up <= '0';
                      down <= '1';
                  else
                      go_to <= whereto_floor;
                      up <= '0';
                      down <= '0';
                  end if;
              when others =>
                  current_state <= s_floor1;
                  up <= '0';
                  down <= '0';                                                      	           
	       end case;
	   end if;
end process;	  
	
--	-- Next state logic ---------------------------------
--	next_state_process : process (current_state, i_floor)
--	begin
	
--	   if (i_floor = "0001") then
--	       next_state <= s_floor1;
--	   elsif (i_floor = "0010") then
--	       next_state <= s_floor2;
--	   elsif (i_floor = "0011") then
--           next_state <= s_floor3;
--       elsif (i_floor = "0100") then
--           next_state <= s_floor4;
--       elsif (i_floor = "0101") then
--           next_state <= s_floor5;
--       elsif (i_floor = "0110") then
--           next_state <= s_floor6;
--       elsif (i_floor = "0111") then
--           next_state <= s_floor7;
--       elsif (i_floor = "1000") then
--           next_state <= s_floor8;
--       elsif (i_floor = "1001") then
--           next_state <= s_floor9;
--       elsif (i_floor = "1010") then
--           next_state <= s_floor10;
--       elsif (i_floor = "1011") then
--           next_state <= s_floor11;
--       elsif (i_floor = "1100") then
--           next_state <= s_floor12;
--       elsif (i_floor = "1101") then
--           next_state <= s_floor13;
--       elsif (i_floor = "1110") then
--           next_state <= s_floor14;
--       elsif (i_floor = "1111") then
--           next_state <= s_floor15;
--       else
--           next_state <= s_floor1;        
--       end if;
--       next_bin <= i_floor;
--    end process;
	
	-- State memory w/ **synchronous** reset ------------
	
	-- reset is active high and will return elevator to floor1
--    clk_process : process (clk_fsm, reset, go)
--    begin
--            if reset = '1' then
--                    current_state <= s_floor1;
----            elsif (rising_edge(clk_fsm)) then
----                    current_state <= next_state;
--            elsif (go = '1') then
--                    current_state <= next_state;
--                    current_updown <= next_updown;  
--            end if;         
--    end process;
	
	
	-- Output logic     ---------------------------------	
	
	-- default is floor1
	
	out_process : process(current_state)
	begin
	   case current_state is
	       
	       when s_floor1 =>
	           o_floor <= "0001";
	       when s_floor2 =>
	           o_floor <= "0010";
	       when s_floor3 =>
	           o_floor <= "0011";
	       when s_floor4 =>
	           o_floor <= "0100";
	       when s_floor5 =>
	           o_floor <= "0101";
	       when s_floor6 =>
	           o_floor <= "0110";
	       when s_floor7 =>
	           o_floor <= "0111";
	       when s_floor8 =>
               o_floor <= "1000";
           when s_floor9 =>
               o_floor <= "1001";
           when s_floor10 =>
               o_floor <= "1010";
           when s_floor11 =>
               o_floor <= "1011";
           when s_floor12 =>
               o_floor <= "1100";
           when s_floor13 =>
               o_floor <= "1101";
           when s_floor14 =>
               o_floor <= "1110";
           when s_floor15 =>
               o_floor <= "1111";
	       when others =>
	           o_floor <= "0001";
	   end case;
    end process;

    o_right <= down;
    o_left <= up;          
end Behavioral;

