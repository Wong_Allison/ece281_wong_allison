----
ECE 281 Lab3 - Advanced Elevator Controller 
----
By C3C Allison Wong

Pre-lab
=======
Top-level Schematic
-------------------
![](images/TopLevelSchematic.JPG)
Mistakes:  
*Ground anode 2, leave 0, 1, and 3   
*16 switches, not 8 and 16 LEDs not 8  
*Need to inverse sevenSegDecoder outputs  

Results
=======

Top-level sketch w/additional functionalities 1 & 2
---------------------------------------------------
![](images/TopLevelv2.JPG)
*forgot to connect sevenSegDecoder to led(6:0)

Top-level sketch of final functionality
---------------------------------------
![](images/TopLevelFinal.JPG)
*forgot to connect anodes and ground extras in schematic  
*also added third clock divider later on

RTL Schematic
-------------
![](images/RTLSchematic.JPG)

Summary
-------
I was able to implement half of the bonus functionality. I only made one elevator but it could pick up a passenger from a given floor and deliver them to the desired floor, while maintaining the functionality of the 4-bit input and the thunderbird lights.

Questions
=========
1. What value for k_DIV do you need to produce a 2Hz clock?  
A1: 25,000,000.

2. Notice the Master Reset does not reset both the FSM and clock like it should. Why not?   
A2: The clock process of the MooreElevatorController was originally made to be synchronous, and since the clock would never reach the rising edge, the state would never change. In order for the master reset to successfully reset both FSM and clock, the clock process must be asynchronous.


3. Bonus functionality: N/A. Didn't get to the second elevator part.


4. What did you learn?  
A4: I learned to plan a top level schematic out, in detail, before implementing the design, or else much confusion may ensue. 

Feedback
========

Number of hours spent on Lab3 : 7 hours

Suggestions to improve Lab3 in future years: None.

Documentation Statement
=======================
None.