--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : alu.vhd
--| AUTHOR(S)     : C3C Allison Wong
--| CREATED       : 20 Feb 2018
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : None yet.
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.std_logic_signed.all;

library unisim;
  use UNISIM.Vcomponents.ALL;

-- entity name should match filename  
entity alu is 
  port(
	a, b : in std_logic_vector(31 downto 0); -- 1-bit input port
	f    : in std_logic_vector(2 downto 0);
	y    : out std_logic_vector(31 downto 0);
	zero : out std_logic  -- 1-bit output port (NOTE: NO semicolon here!)
  );
end alu;

architecture alu_arch of alu is 
	
	signal c_B_not : std_logic_vector(31 downto 0);
	signal c_B_mux2 : std_logic_vector(31 downto 0);
	signal c_and : std_logic_vector(31 downto 0);
	signal c_or : std_logic_vector(31 downto 0);
	signal c_add : std_logic_vector(31 downto 0);
	signal c_zero : std_logic_vector(31 downto 0) := x"00000000";
	signal c_mux4 : std_logic_vector(31 downto 0);  
	
begin
    c_B_not  <= not b;
    c_B_mux2 <= c_B_not when f(2) = '1' else
                b;
    c_and    <= a and c_B_mux2;
	c_or     <= a or c_B_mux2;
	c_add    <= a + c_B_mux2 + f(2);
	c_zero(0)<= c_add(31);
	
	c_mux4 <= c_and when f(1) = '0' and f(0) = '0' else
	       c_or when f(1) = '0' and f(0) = '1' else
	       c_add when f(1) = '1' and f(0) = '0' else
	       c_zero;
	y <= c_mux4;
    zero <= '1' when (c_mux4 = x"00000000") else '0';
end alu_arch;
