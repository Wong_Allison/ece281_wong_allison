--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : myXOR_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : C3C Allison Wong
--| CREATED       : 20 Feb 2018
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : None.
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity alu_tb is
end alu_tb;

architecture test_bench of alu_tb is 
	
  -- declare the component of your top-level design unit under test (UUT)
  component alu is 
    port(
      a, b : in std_logic_vector(31 downto 0); -- 1-bit input port
      f    : in std_logic_vector(2 downto 0);
      y    : out std_logic_vector(31 downto 0);
      zero : out std_logic  -- 1-bit output port (NOTE: NO semicolon here!)
    );
  end component alu;
  
  -- declare signals needed to stimulate the UUT inputs
  signal a : std_logic_vector(31 downto 0) := (others => '0');
  signal b : std_logic_vector(31 downto 0) := (others => '0');
  signal f : std_logic_vector(2 downto 0) := (others => '0');
  
  signal y : std_logic_vector(31 downto 0);
  signal zero : std_logic;
  
begin
	-- PORT MAPS ----------------------------------------

	-- map ports for any component instances (port mapping is like wiring hardware)
	uut: alu port map (
		a => a,
		b => b,
		f => f,
		y => y,
		zero => zero
	);

    stim_proc: process
    begin
            
            -- ADD
            f <= "010"; a <= x"00000000"; b <= x"00000000";
            wait for 10ns;
            ASSERT y = x"00000000" REPORT "TEST 001.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '1' REPORT "TEST 001.1 FAILED" SEVERITY ERROR;
            
            f <= "010"; a <= x"00000000"; b <= x"FFFFFFFF";
            wait for 10ns;
            ASSERT y = x"FFFFFFFF" REPORT "TEST 002.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 002.1 FAILED" SEVERITY ERROR;

	           
            f <= "010"; a <= x"00000001"; b <= x"FFFFFFFF";
            wait for 10ns;
            ASSERT y = x"00000000" REPORT "TEST 003.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '1' REPORT "TEST 003.1 FAILED" SEVERITY ERROR;

            f <= "010"; a <= x"000000FF"; b <= x"00000001";
            wait for 10ns;
            ASSERT y = x"00000100" REPORT "TEST 004.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 004.1 FAILED" SEVERITY ERROR;
           
           -- SUB
 
            f <= "110"; a <= x"00000000"; b <= x"00000000";
            wait for 10ns;
            ASSERT y = x"00000000" REPORT "TEST 005.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '1' REPORT "TEST 005.1 FAILED" SEVERITY ERROR;
                       
            f <= "110"; a <= x"00000000"; b <= x"FFFFFFFF";
            wait for 10ns;
            ASSERT y = x"00000001" REPORT "TEST 006.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 006.1 FAILED" SEVERITY ERROR;
  
            f <= "110"; a <= x"00000001"; b <= x"00000001";
            wait for 10ns;
            ASSERT y = x"00000000" REPORT "TEST 007.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '1' REPORT "TEST 007.1 FAILED" SEVERITY ERROR;
                    
            f <= "110"; a <= x"00000100"; b <= x"00000001";
            wait for 10ns;
            ASSERT y = x"000000FF" REPORT "TEST 008.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 008.1 FAILED" SEVERITY ERROR;
            
            -- SLT
             
            f <= "111"; a <= x"00000000"; b <= x"00000000";
            wait for 10ns;
            ASSERT y = x"00000000" REPORT "TEST 009.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '1' REPORT "TEST 009.1 FAILED" SEVERITY ERROR;
                         
            f <= "111"; a <= x"00000000"; b <= x"00000001";
            wait for 10ns;
            ASSERT y = x"00000001" REPORT "TEST 010.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 010.1 FAILED" SEVERITY ERROR;
                        
            f <= "111"; a <= x"00000000"; b <= x"FFFFFFFF";
            wait for 10ns;
            ASSERT y = x"00000000" REPORT "TEST 011.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '1' REPORT "TEST 011.1 FAILED" SEVERITY ERROR;

            f <= "111"; a <= x"00000001"; b <= x"00000000";
            wait for 10ns;
            ASSERT y = x"00000000" REPORT "TEST 012.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '1' REPORT "TEST 012.1 FAILED" SEVERITY ERROR;
                  
            f <= "111"; a <= x"FFFFFFFF"; b <= x"00000000";
            wait for 10ns;
            ASSERT y = x"00000001" REPORT "TEST 013.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 013.1 FAILED" SEVERITY ERROR;

            -- AND
            
            f <= "000"; a <= x"FFFFFFFF"; b <= x"FFFFFFFF";
            wait for 10ns;
            ASSERT y = x"FFFFFFFF" REPORT "TEST 014.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 014.1 FAILED" SEVERITY ERROR;      
            
            f <= "000"; a <= x"FFFFFFFF"; b <= x"12345678";
            wait for 10ns;
            ASSERT y = x"12345678" REPORT "TEST 015.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 015.1 FAILED" SEVERITY ERROR;      
                        
            f <= "000"; a <= x"12345678"; b <= x"87654321";
            wait for 10ns;
            ASSERT y = x"02244220" REPORT "TEST 016.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 016.1 FAILED" SEVERITY ERROR;      
                        
            f <= "000"; a <= x"00000000"; b <= x"FFFFFFFF";
            wait for 10ns;
            ASSERT y = x"00000000" REPORT "TEST 017.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '1' REPORT "TEST 017.1 FAILED" SEVERITY ERROR;      
            
            -- AND NOT
            
            f <= "100"; a <= x"12345678"; b <= x"02040608";
            wait for 10ns;
            ASSERT y = x"10305070" REPORT "TEST 018.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 018.1 FAILED" SEVERITY ERROR;   
                        
            f <= "100"; a <= x"AAAAAAAA"; b <= x"FFFF0000";
            wait for 10ns;
            ASSERT y = x"0000AAAA" REPORT "TEST 019.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 019.1 FAILED" SEVERITY ERROR;   
            
            f <= "100"; a <= x"FFFFFFFF"; b <= x"FFFFFFFF";
            wait for 10ns;
            ASSERT y = x"00000000" REPORT "TEST 020.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '1' REPORT "TEST 020.1 FAILED" SEVERITY ERROR;                  
             
            f <= "100"; a <= x"FFFFFFFF"; b <= x"12345678";
            wait for 10ns;
            ASSERT y = x"EDCBA987" REPORT "TEST 021.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 021.1 FAILED" SEVERITY ERROR;    
            
            -- OR
            f <= "001"; a <= x"FFFFFFFF"; b <= x"FFFFFFFF";
            wait for 10ns;
            ASSERT y = x"FFFFFFFF" REPORT "TEST 022.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 022.1 FAILED" SEVERITY ERROR;

            f <= "001"; a <= x"12345678"; b <= x"87654321";
            wait for 10ns;
            ASSERT y = x"97755779" REPORT "TEST 023.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 023.1 FAILED" SEVERITY ERROR;
            
            f <= "001"; a <= x"00000000"; b <= x"FFFFFFFF";
            wait for 10ns;
            ASSERT y = x"FFFFFFFF" REPORT "TEST 024.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 024.1 FAILED" SEVERITY ERROR;
            
            f <= "001"; a <= x"00000000"; b <= x"00000000";
            wait for 10ns;
            ASSERT y = x"00000000" REPORT "TEST 025.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '1' REPORT "TEST 025.1 FAILED" SEVERITY ERROR;     
                   
            -- OR NOT
            f <= "101"; a <= x"00000000"; b <= x"00000000";
            wait for 10ns;
            ASSERT y = x"FFFFFFFF" REPORT "TEST 026.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 026.1 FAILED" SEVERITY ERROR;
            
            f <= "101"; a <= x"00000000"; b <= x"FFFFFFFF";
            wait for 10ns;
            ASSERT y = x"00000000" REPORT "TEST 027.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '1' REPORT "TEST 027.1 FAILED" SEVERITY ERROR;
            
            f <= "101"; a <= x"12345678"; b <= x"87654321";
            wait for 10ns;
            ASSERT y = x"7ABEFEFE" REPORT "TEST 028.0 FAILED" SEVERITY ERROR;
            ASSERT zero = '0' REPORT "TEST 028.1 FAILED" SEVERITY ERROR;
            
            report "All test cases done." severity note;
		
		wait; -- wait forever
	end process;	
	-----------------------------------------------------	
	
end test_bench;
