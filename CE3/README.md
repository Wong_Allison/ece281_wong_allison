----
ECE 281 CE3 - 32-bit ALU
----

By C3C Allison Wong

Objectives
==========

-   Design, write, and test a 32-bit Arithmetic Logic Unit (ALU) that will be used for             future assignments when building a fully-functional MIPS microprocessor

-   Design and write a self-checking testbench

Preliminary Design
==================

ALU Schematic
-------------
![AnnotatedSchematic.JPG](C:/Users/C20Allison.Wong/Documents/USAFA/C3C_2nd_Semester/ECE281/ece281_wong_allison/CE3/images/AnnotatedSchematic.JPG)

Table of Functions
------------------

| F2:0 | Function |
|------|----------|
| 000  | A & B    |
| 001  | A \|B    |
| 010  | A + B    |
| 011  | not used |
| 100  | A & \~B  |
| 101  | A \| \~B |
| 110  | A - B    |
| 111  | SLT      |

Test Vector Table
-----------------

| \# | Test                         | F[2:0] | A        | B        | Y        | Zero |
|----|------------------------------|--------|----------|----------|----------|------|
| 1  | ADD 0+0                      | 2      | 00000000 | 00000000 | 00000000 | 1    |
| 2  | ADD 0+(-1)                   | 2      | 00000000 | FFFFFFFF | FFFFFFFF | 0    |
| 3  | ADD 1+(-1)                   | 2      | 00000001 | FFFFFFFF | 00000000 | 1    |
| 4  | ADD 0xFF+1                   | 2      | 000000FF | 00000001 | 00000100 | 0    |
| 5  | SUB 0-0                      | 6      | 00000000 | 00000000 | 00000000 | 1    |
| 6  | SUB 0-(-1)                   | 6      | 00000000 | FFFFFFFF | 00000001 | 0    |
| 7  | SUB 1-1                      | 6      | 00000001 | 00000001 | 00000000 | 1    |
| 8  | SUB 0x100-1                  | 6      | 00000100 | 00000001 | 000000FF | 0    |
| 9  | SLT 0,0                      | 7      | 00000000 | 00000000 | 00000000 | 1    |
| 10 | SLT 0,1                      | 7      | 00000000 | 00000001 | 00000001 | 0    |
| 11 | SLT 0,-1                     | 7      | 00000000 | FFFFFFFF | 00000000 | 1    |
| 12 | SLT 1,0                      | 7      | 00000001 | 00000000 | 00000000 | 1    |
| 13 | SLT -1,0                     | 7      | FFFFFFFF | 00000000 | 00000001 | 0    |
| 14 | AND 0xFFFFFFFF, 0xFFFFFFFF   | 0      | FFFFFFFF | FFFFFFFF | FFFFFFFF | 0    |
| 15 | AND 0xFFFFFFFF, 0x12345678   | 0      | FFFFFFFF | 12345678 | 12345678 | 0    |
| 16 | AND 0x12345678, 0x87654321   | 0      | 12345678 | 87654321 | 02244220 | 0    |
| 17 | AND 0x00000000, 0xFFFFFFFF   | 0      | 00000000 | FFFFFFFF | 00000000 | 1    |
| 18 | AND 0x12345678, \~0x02040608 | 4      | 12345678 | 02040608 | 10305070 | 0    |
| 19 | AND 0xAAAAAAAA, \~0xFFFF0000 | 4      | AAAAAAAA | FFFF0000 | 0000AAAA | 0    |
| 20 | AND 0xFFFFFFFF, \~0xFFFFFFFF | 4      | FFFFFFFF | FFFFFFFF | 00000000 | 1    |
| 21 | AND 0xFFFFFFFF, \~0x12345678 | 4      | FFFFFFFF | 12345678 | EDCBA987 | 0    |
| 22 | OR 0xFFFFFFFF, 0xFFFFFFFF    | 1      | FFFFFFFF | FFFFFFFF | FFFFFFFF | 0    |
| 23 | OR 0x12345678, 0x87654321    | 1      | 12345678 | 87654321 | 97755779 | 0    |
| 24 | OR 0x00000000, 0xFFFFFFFF    | 1      | 00000000 | FFFFFFFF | FFFFFFFF | 0    |
| 25 | OR 0x00000000, 0x00000000    | 1      | 00000000 | 00000000 | 00000000 | 1    |
| 26 | OR 0x00000000, \~0x00000000  | 5      | 00000000 | 00000000 | FFFFFFFF | 0    |
| 27 | OR 0x00000000, \~0xFFFFFFFF  | 5      | 00000000 | FFFFFFFF | 00000000 | 1    |
| 28 | OR 0x12345678, \~0x87654321  | 5      | 12345678 | 87654321 | 7ABEFEFE | 0    |

Additional Test Vectors
-----------------------
Additional test vectors have been added as lines 16, 17, 20, 21, 24, 25, 27, and 28.

Results
=======

Tcl Console Window
------------------
Success.
![TCLmessagebox.JPG](images/TCLmessagebox.JPG)

Hiearchical RTL Component Report
--------------------------------
![HierarchicalRTLComponentReport.JPG](images/HierarchicalRTLComponentReport.JPG)

Utilization Summary
-------------------
![UtilizationSummary.JPG](images/UtilizationSummary.JPG)

Questions
---------
Q1: Do the number and type of components in the synthesis report agree with your intended design?

A1: The intended design has one adder, one 2:1 mux, and one 4:1 mux. Since the 4:1 mux is represented with three 2:1 muxes, the synthesis report reported four 2:1 muxes.

Q2: Does the number of input/output buffers make sense based on your design?  Explain.

A2: The number of input/output buffers does make sense. A has 32 bits. B has 32 bits. F has 3 bits. Y has 32 bits. Z has 1 bit. That adds up to 100 input and output bits utilized, as shown in the screenshot above in the utilization summary.

Feedback
========

Number of hours spent on CE3: 6 hours

Suggestions to improve CE3 in future years: Spend just a bit more time in class explaining the process.


Documentation Statement
=======================
Talked with C3C Tricia Dang about approaching the design of the signals in the alu architecture.