----
ECE 281 Lab4 - MIPS Single-Cycle Processor
----

By C3C Allison Wong

Objectives
==========
- To build a simplified MIPS single-cycle processor using VHDL, by combining the ALU you previously created with the code for the rest of the processor
- To implement two new instructions and confirm they work by utilizing a second test program
- To thoroughly understand the internal operation of the MIPS single-cycle processor

Preliminary Design
==================

![assemblycode](images/AssemblyCode.JPG)

The assembly code, shown above, was used to fill out the table below.

![prelabtable](images/PreLabTableAgain.JPG)

What address will the final sw instruction write to and what value will it write?

***It will write the value of $2, which is 7, to 0x00000084.***

Results
=======
Milestone 1
-----------

![milestone1](images/Milestone1.JPG)

The results were as expected. The outputs match those in the table. The last WriteData value was 7.

Milestone 2
-----------

![milestone2](images/Milestone2.JPG)
This schematic includes added hardware necessary for ori and bne to function.
* Missing connecting signal from ALUSrc mux to new ori mux, and signal from NOT gate to bne mux 


For bne, there is a mux that extends off the Zero to determine whether to use zero or not_zero. A NOT gate was necessary as well. A bne op wire connects this mux to the controller.

For ori, first there is a zero extend to make the immediate the appropriate length before entering the ALU. Then, there is a mux that determines whether to use that zero-extended immediate, or the original input. An ori op wire connects this mux to the Controller.

What address will the sw instruction write to and what value will it write?

***It will write the value of $t3, which is 2, to 0xffff7f54.***

![assemblycode2](images/AssemblyCode2.JPG)

The assembly code, shown above, along with my schematic, was used to fill out the three tables below.

![milestone2table](images/Milestone2Table.JPG)

![milestone2table2](images/Milestone2Table2.JPG)

Columns were added to this table to account for the newly created ori and bne control signals. The ori and bne command rows were also added

![milestone2table3](images/Milestone2Table3.JPG)

Milestone 3
-----------

While working on Milestone 3, I completely redesigned my program, reflected in the schematic below. 

![newschematic](images/NewSchematic.JPG)

In the old design, I had two new muxes that were controlled by signals bne and ori in the controller. This new design still requires a zero extend, but the zeroimm signal runs into the existing SrcB mux, changing the ALUSrc signal to 2 bits, taking care of the ori functionality. 

For bne, a new bne op wire was still added to the control unit, and will be 1 when a bne instruction is given. As a result of the added gates, the PC source will change if either 1) bne is 1, and the two values are not equal, or 2) Branch is 1, and the two values are equal. The bne signal and Branch are never expected to be 1 at the same time.  

Using the new schematic, I edited the main decoder table, shown below. 

![newmilestone2table2](images/NewMilestone2Table2.JPG)

The results were as expected, shown in the waveforms below. The first waveform is when the instructions from lab4_mipstest.asm was ran. The second is when lab4_mipstest2.asm's instructions were ran. Both instructions' results were as expected, matching their respective tables.

memfile.dat:

![memfile](images/Milestone3_1.JPG)

memfile2.dat:

![memfile2](images/Milestone3.JPG)

Feedback
========

Number of hours spent on Lab 4 : 5 hours

Suggestions to improve Lab 4 in future years: None. It was a good lab.

Documentation Statement
=======================
None.
