# lab4_mipstest2.asm
# Test MIPS instructions ori and bne.

#Assembly Code 		               	          	
main:	ori	$t0, $0,  0x8000   # initialize $t0 = 0x8000
	addi	$t1, $0,  -32768   # initialize $t1 = 0xFFFF8000
	ori	$t2, $t0, 0x8001   # initialize $t2 = 0x8001
	beq	$t0, $t1, there    # nottaken
	slt	$t3, $t1, $t0      # $t3 = -32768 < 0x8000 = 1
	bne	$t3, $0,  here     # should be taken
	j	there              # skip
here:	sub	$t2, $t2, $t0      # $t2 = 0x8001-0x8000 = 1
	ori	$t0, $t0, 0xFF     # $t0 = 0x8000 OR 0xFF = 0x80FF
there:	add	$t3, $t3, $t2      # $t3 = 1+1 = 2
	sub	$t0, $t2, $t0      # $t0 = 1-0x80FF = 0xFFFF7F02
	sw	$t3, 82($t0)       # [0xFFFF7F02 + 0x52] = [$t3]
