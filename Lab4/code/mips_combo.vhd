---------------------------------------------------------------------------------------
-- mips.vhd
-- David_Harris@hmc.edu and Sarah_Harris@hmc.edu 30 May 2006
-- Single Cycle MIPS processor
-- Modified by Ryan Silva
-- - to work in VHDL 93
--	Modified by Danial Neebel Jul 2014
-- - to add imem, and dmem modules 
-- Modified by Virginia Trimble Apr 2015
-- - to add the top module and remove alu 

-- Modified by Capt Phillip Warner Apr 2017/2018
-- - Changed ALU entity port names to match CE3 signal names
-- - Created internal wires so that ALU y output does not have to be inout or buffer
-- - Removed uneccessary ALU VHDL.
--   -- Since entity now matches CE3, the module file simply needs to be added to the project
-- - Removed incorrect comments on data memory
-- - Provided default data memory (0xECE28100) to avoid 'U' in ReadData
-- - Changed signal names to match textbook diagrams used in class
-- - Port maps now use explicit bindings
-- - Formatting clean-up

-- TODO: - Remove remaining inout ports by creating internal wires and connecting them properly
--       - Split this file into multiple proper VHDL files
---------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------
-- Entity Declarations
---------------------------------------------------------------------------------------

library IEEE; use IEEE.STD_LOGIC_1164.all;
entity mips is -- single cycle MIPS processor
  port(clk, reset:        in  STD_LOGIC;
       PC:                inout STD_LOGIC_VECTOR(31 downto 0);
       Instr:             in  STD_LOGIC_VECTOR(31 downto 0);
       MemWrite:          out STD_LOGIC;
       ALUResult, WriteData: inout STD_LOGIC_VECTOR(31 downto 0);
       ReadData:          in  STD_LOGIC_VECTOR(31 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all;
entity controller is -- single cycle control decoder
  port(Op, Funct:          in  STD_LOGIC_VECTOR(5 downto 0);
       Zero:               in  STD_LOGIC;
       MemtoReg, MemWrite: out STD_LOGIC;
       PCSrc: 			   out STD_LOGIC;
	   ALUSrc:      	   out STD_LOGIC_VECTOR(1 downto 0); --addORI
       RegDst, RegWrite:   out STD_LOGIC;
       Jump:               out STD_LOGIC;
       ALUControl:         out STD_LOGIC_VECTOR(2 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all;
entity maindec is -- main control decoder
  port(Op:                 in  STD_LOGIC_VECTOR(5 downto 0);
       MemtoReg, MemWrite: out STD_LOGIC;
       Branch:     		   out STD_LOGIC;
       Bne   :             out STD_LOGIC;
	   ALUSrc:      	   out STD_LOGIC_VECTOR(1 downto 0);
       RegDst, RegWrite:   out STD_LOGIC;
       Jump:               out STD_LOGIC;
       ALUOp:              out  STD_LOGIC_VECTOR(1 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all;
entity aludec is -- ALU control decoder
  port(Funct:      in  STD_LOGIC_VECTOR(5 downto 0);
       ALUOp:      in  STD_LOGIC_VECTOR(1 downto 0);
       ALUControl: out STD_LOGIC_VECTOR(2 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all; use IEEE.STD_LOGIC_ARITH.all;
entity datapath is  -- MIPS datapath
  port(clk, reset:        in  STD_LOGIC;
       MemtoReg, PCSrc:   in  STD_LOGIC;
	   ALUSrc:      	  in STD_LOGIC_VECTOR(1 downto 0); 
       RegDst:    		  in  STD_LOGIC;
       RegWrite, Jump:    in  STD_LOGIC;
       ALUControl:        in  STD_LOGIC_VECTOR(2 downto 0);
       Zero:              out STD_LOGIC;
       PC:                inout STD_LOGIC_VECTOR(31 downto 0);
       Instr:             in  STD_LOGIC_VECTOR(31 downto 0);
       dpALUResult, WriteData: inout STD_LOGIC_VECTOR(31 downto 0);
       ReadData:          in  STD_LOGIC_VECTOR(31 downto 0));
end;

--your ALU entity declaration is included in a separate file (alu.vhd)

library IEEE; use IEEE.STD_LOGIC_1164.all; use IEEE.STD_LOGIC_UNSIGNED.all;
entity regfile is -- three-port register file
  port(clk:           in  STD_LOGIC;
       we3:           in  STD_LOGIC;
       ra1, ra2, wa3: in  STD_LOGIC_VECTOR(4 downto 0);
       wd3:           in  STD_LOGIC_VECTOR(31 downto 0);
       rd1, rd2:      out STD_LOGIC_VECTOR(31 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all; use IEEE.STD_LOGIC_UNSIGNED.all;
entity adder is -- adder
  port(a, b: in  STD_LOGIC_VECTOR(31 downto 0);
       y:    out STD_LOGIC_VECTOR(31 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all;
entity shiftLeft2 is -- shift left by 2
  port(a: in  STD_LOGIC_VECTOR(31 downto 0);
       y: out STD_LOGIC_VECTOR(31 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all;
entity signext is -- sign extender
  port(a: in  STD_LOGIC_VECTOR(15 downto 0);
       y: out STD_LOGIC_VECTOR(31 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all;
entity zeroext is -- sign extender
  port(a: in  STD_LOGIC_VECTOR(15 downto 0);
       y: out STD_LOGIC_VECTOR(31 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all;  use IEEE.STD_LOGIC_ARITH.all;
entity flopr is -- flip-flop with synchronous reset
  generic(width: integer);
  port(clk, reset: in  STD_LOGIC;
       d:          in  STD_LOGIC_VECTOR(width-1 downto 0);
       q:          out STD_LOGIC_VECTOR(width-1 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all;
entity mux2 is -- two-input multiplexer
  generic(width: integer);
  port(d0, d1: in  STD_LOGIC_VECTOR(width-1 downto 0);
       s:      in  STD_LOGIC;
       y:      out STD_LOGIC_VECTOR(width-1 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all; 
entity mux3 is -- three-input multiplexer
  generic(width: integer);
  port(d0, d1, d2 : in  STD_LOGIC_VECTOR(width-1 downto 0);
       s:      in  STD_LOGIC_VECTOR(1 downto 0);
       y:      out STD_LOGIC_VECTOR(width-1 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all; use IEEE.NUMERIC_STD.all;
entity top is -- top-level design for testing
  port(clk, reset:           in     STD_LOGIC;
       WriteData, DataAdr:   inout STD_LOGIC_VECTOR(31 downto 0);
       MemWrite:             inout STD_LOGIC);
end;

library IEEE; use IEEE.STD_LOGIC_1164.all; use STD.TEXTIO.all;
use IEEE.NUMERIC_STD.all;
entity dmem is -- data memory
  port(clk, we:  in STD_LOGIC;
       a, wd:    in STD_LOGIC_VECTOR(31 downto 0);
       rd:       out STD_LOGIC_VECTOR(31 downto 0));
end;

library IEEE; use IEEE.STD_LOGIC_1164.all; use STD.TEXTIO.all;
use IEEE.NUMERIC_STD.all; use IEEE.STD_LOGIC_UNSIGNED.all; use IEEE.STD_LOGIC_ARITH.all;
entity imem is -- instruction memory
  port(a:  in  STD_LOGIC_VECTOR(5 downto 0);
       rd: out STD_LOGIC_VECTOR(31 downto 0));
end;

---------------------------------------------------------------------------------------
-- Architecture Definitions
---------------------------------------------------------------------------------------

architecture struct of mips is
  component controller
    port(Op, Funct:          in  STD_LOGIC_VECTOR(5 downto 0);
         Zero:               in  STD_LOGIC;
         MemtoReg, MemWrite: out STD_LOGIC;
         PCSrc:			     out STD_LOGIC;
		 ALUSrc:      	     out STD_LOGIC_VECTOR(1 downto 0);
         RegDst, RegWrite:   out STD_LOGIC;
         Jump:               out STD_LOGIC;
         ALUControl:         out STD_LOGIC_VECTOR(2 downto 0));
  end component;

  component datapath
    port(clk, reset:        in  STD_LOGIC;
         MemtoReg, PCSrc:   in  STD_LOGIC;
		 ALUSrc:      	    in 	STD_LOGIC_VECTOR(1 downto 0); 
         RegDst:   			in  STD_LOGIC;
         RegWrite, Jump:    in  STD_LOGIC;
         ALUControl:        in  STD_LOGIC_VECTOR(2 downto 0);
         Zero:              out STD_LOGIC;
         PC:                inout STD_LOGIC_VECTOR(31 downto 0);
         Instr:             in 	STD_LOGIC_VECTOR(31 downto 0);
         dpALUResult, WriteData: inout STD_LOGIC_VECTOR(31 downto 0);
         ReadData:          in  STD_LOGIC_VECTOR(31 downto 0));
  end component;

  signal MemtoReg, RegDst, RegWrite, Jump, PCSrc: STD_LOGIC;
  signal ALUSrc: std_logic_vector(1 downto 0);  
  signal Zero: STD_LOGIC;
  signal ALUControl: STD_LOGIC_VECTOR(2 downto 0);

begin
  cont: controller
  port map( Op         => Instr(31 downto 26),
			Funct      => Instr(5 downto 0),
			Zero       => Zero,
			MemtoReg   => MemtoReg,
			MemWrite   => MemWrite,
			PCSrc      => PCSrc,
			ALUSrc     => ALUSrc,
			RegDst     => RegDst,
			RegWrite   => RegWrite,
			Jump       => Jump,
			ALUControl => ALUControl
		  );

  dp: datapath
  port map( clk         => clk,
			reset       => reset,
			MemtoReg    => MemtoReg,
			PCSrc       => PCSrc,
			ALUSrc      => ALUSrc,	
			RegDst      => RegDst,
			RegWrite    => RegWrite, 
			Jump        => Jump, 
			ALUControl  => ALUControl,
			Zero        => Zero, 
			PC          => PC,     
			Instr       => Instr,
			dpALUResult => ALUResult,
			WriteData   => WriteData, 
			ReadData    => ReadData 
		  );
end;


architecture struct of controller is
  component maindec
    port(Op:                 in  STD_LOGIC_VECTOR(5 downto 0);
         MemtoReg, MemWrite: out STD_LOGIC;
         Branch:     		 out STD_LOGIC;
         Bne   :             out STD_LOGIC;
		 ALUSrc:      		 out STD_LOGIC_VECTOR(1 downto 0); 
         RegDst, RegWrite:   out STD_LOGIC;
         Jump:               out STD_LOGIC;
         ALUOp:              out  STD_LOGIC_VECTOR(1 downto 0));
  end component;
  
  component aludec
    port(Funct:      in  STD_LOGIC_VECTOR(5 downto 0);
         ALUOp:      in  STD_LOGIC_VECTOR(1 downto 0);
         ALUControl: out STD_LOGIC_VECTOR(2 downto 0));
  end component;
    
  signal ALUOp: STD_LOGIC_VECTOR(1 downto 0);
  signal Branch: STD_LOGIC;
  signal bne   : STD_LOGIC;
  
begin
  md: maindec port map( Op        => Op,
						MemtoReg  => MemtoReg,
						MemWrite  => MemWrite,
						Branch    => Branch,
						Bne       => Bne,
                        ALUSrc    => ALUSrc,
						RegDst    => RegDst,
						RegWrite  => RegWrite,
						Jump      => Jump,
						ALUOp     => ALUOp
					  );
  
  ad: aludec port map( Funct	  => Funct,
					   ALUOp      => ALUOp,
					   ALUControl => ALUControl
					 );

  PCSrc <= (Branch and Zero) or (bne and (not Zero));

end;


architecture behave of maindec is
  signal controls: STD_LOGIC_VECTOR(10 downto 0);

begin
  process(Op) begin
    case Op is							
      when "000000" => controls <= "01100000010"; -- Rtype
      when "100011" => controls <= "01001001000"; -- LW
      when "101011" => controls <= "00001010000"; -- SW
      when "000100" => controls <= "00000100001"; -- BEQ
      when "001000" => controls <= "01001000000"; -- ADDI
      when "000010" => controls <= "00000000100"; -- J
      when "001101" => controls <= "01010000011"; -- ORI
      when "000101" => controls <= "10000000001"; -- BNE      
      when others   => controls <= "-----------"; -- illegal Op
    end case;
  end process;

  bne      <= controls(10);
  RegWrite <= controls(9);
  RegDst   <= controls(8);
  ALUSrc   <= controls(7 downto 6);  
  Branch   <= controls(5);
  MemWrite <= controls(4);
  MemtoReg <= controls(3);
  Jump     <= controls(2);
  ALUOp    <= controls(1 downto 0);
end;


architecture behave of aludec is
begin
  process(ALUOp, Funct) begin
    case ALUOp is
      when "00" => ALUControl <= "010"; -- add (for lb/sb/addi)
      when "01" => ALUControl <= "110"; -- sub (for beq)
      when "11" => ALUControl <= "001"; -- or (for ori)
      when others => case Funct is         -- R-type instructions
                         when "100000" => ALUControl <= "010"; -- add (for add)
                         when "100010" => ALUControl <= "110"; -- subtract (for sub)
                         when "100100" => ALUControl <= "000"; -- logical and (for and)
                         when "100101" => ALUControl <= "001"; -- logical or (for or)
                         when "101010" => ALUControl <= "111"; -- set on less (for slt)
                         when others   => ALUControl <= "---"; -- should never happen
                     end case;
    end case;
  end process;
end;


architecture struct of datapath is
  component alu
    port(a, b   :  in  STD_LOGIC_VECTOR(31 downto 0);
         f      :  in  STD_LOGIC_VECTOR(2 downto 0);
         y      :  out STD_LOGIC_VECTOR(31 downto 0);
	     Zero   :  out STD_LOGIC);
  end component;
  
  component regfile
    port(clk:           in  STD_LOGIC;
         we3:           in  STD_LOGIC;
         ra1, ra2, wa3: in  STD_LOGIC_VECTOR(4 downto 0);
         wd3:           in  STD_LOGIC_VECTOR(31 downto 0);
         rd1, rd2:      out STD_LOGIC_VECTOR(31 downto 0));
  end component;
  
  component adder
    port(a, b: in  STD_LOGIC_VECTOR(31 downto 0);
         y:    out STD_LOGIC_VECTOR(31 downto 0));
  end component;
  
  component shiftLeft2
    port(a: in  STD_LOGIC_VECTOR(31 downto 0);
         y: out STD_LOGIC_VECTOR(31 downto 0));
  end component;
  
  component signext
    port(a: in  STD_LOGIC_VECTOR(15 downto 0);
         y: out STD_LOGIC_VECTOR(31 downto 0));
  end component;
  
  component zeroext
    port(a: in STD_LOGIC_VECTOR(15 downto 0);
         y: out STD_LOGIC_VECTOR(31 downto 0));
  end component;       
  
  component flopr generic(width: integer);
    port(clk, reset: in  STD_LOGIC;
         d:          in  STD_LOGIC_VECTOR(width-1 downto 0);
         q:          out STD_LOGIC_VECTOR(width-1 downto 0));
  end component;
  
  component mux2 generic(width: integer);
    port(d0, d1: in  STD_LOGIC_VECTOR(width-1 downto 0);
         s:      in  STD_LOGIC;
         y:      out STD_LOGIC_VECTOR(width-1 downto 0));
  end component;
  
  component mux3 generic(width: integer);
    port(d0, d1, d2: in STD_LOGIC_VECTOR(width-1 downto 0);
         s: in STD_LOGIC_VECTOR(1 downto 0);
         y: out STD_LOGIC_VECTOR(width-1 downto 0));
  end component;
  
  signal writereg: STD_LOGIC_VECTOR(4 downto 0);
  signal PCjump, PCnext, PCnextbr, PCPlus4, PCBranch: STD_LOGIC_VECTOR(31 downto 0);
  signal signimm, signimmsh, zeroimm: STD_LOGIC_VECTOR(31 downto 0); 
  signal SrcA, SrcB, ALUResult: STD_LOGIC_VECTOR(31 downto 0); 
  signal Result: STD_LOGIC_VECTOR(31 downto 0);

begin
  -- next PC logic
  PCjump <= PCPlus4(31 downto 28) & Instr(25 downto 0) & "00";
  
  PCreg: flopr generic map(32)
			   port map( clk 	=> clk, 
						 reset 	=> reset, 
						 d 	   	=> PCnext, 
						 q 		=> PC
					   );
  
  PCadd1: adder port map( a => PC, 
						  b => X"00000004", 
						  y => PCPlus4
						);
  
  immsh: shiftLeft2 port map( a => signimm, 
							  y => signimmsh
					        );
  
  PCadd2: adder port map( a => PCPlus4, 
						  b => signimmsh, 
						  y => PCBranch
						);
  
  PCbrmux: mux2 generic map(32)
				port map( d0 => PCPlus4, 
						  d1 => PCBranch, 
						  s  => PCSrc, 
						  y  => PCnextbr
						);
  
  PCmux: mux2 generic map(32)
			  port map( d0 => PCnextbr, 
						d1 => PCjump, 
						s  => Jump, 
						y  => PCnext
					  );					  

					  
  -- register file logic
  rf: regfile port map( clk => clk, 
						we3 => RegWrite, 
						ra1 => Instr(25 downto 21), 
						ra2 => Instr(20 downto 16),
						wa3 => writereg, 
						wd3 => Result, 
						rd1 => SrcA, 
						rd2 => WriteData
					  );
			  
  wrmux: mux2 generic map(5) 
			  port map( d0 => Instr(20 downto 16), 
						d1 => Instr(15 downto 11),
                        s  => RegDst, 
						y  => writereg
					  );
					  
  resmux: mux2 generic map(32)
			   port map( d0 => ALUResult, 
						 d1 => ReadData, 
						 s  => MemtoReg, 
						 y  => Result
					   );
  
  se: signext port map( a => Instr(15 downto 0), 
						y => signimm
					  );
	
  ze: zeroext port map( a => Instr(15 downto 0),
                        y => zeroimm
                      );


  -- ALU logic
  SrcBmux: mux3 generic map(32)
				port map( d0 => WriteData, 
						  d1 => signimm, 
						  d2 => zeroimm,
						  s  => ALUSrc, 
						  y  => SrcB
						); 
    
  mainalu:  alu port map( a 	 => SrcA, 
						  b 	 => SrcB, 
						  f      => ALUControl, 
						  y 	 => ALUResult, 
						  zero 	 => Zero
						);

  dpALUResult <= ALUResult;
  
end;


-- ALU architecture is defined in a separate file (alu.vhd).


architecture behave of regfile is
  type ramtype is array (31 downto 0) of STD_LOGIC_VECTOR(31 downto 0);
  signal mem: ramtype;

begin
  -- three-ported register file
  -- read two ports combinationally
  -- write third port on rising edge of clock
  process(clk) begin
    if rising_edge(clk) then
       if we3 = '1' then mem(CONV_INTEGER(wa3)) <= wd3;
       end if;
    end if;
  end process;
  
  process(ra1, ra2, mem) begin
    if (conv_integer(ra1) = 0) then rd1 <= X"00000000"; -- register 0 holds 0
    else rd1 <= mem(CONV_INTEGER(ra1));
    end if;
    if (conv_integer(ra2) = 0) then rd2 <= X"00000000"; 
    else rd2 <= mem(CONV_INTEGER(ra2));
    end if;
  end process;
end;


architecture behave of adder is
begin
  y <= a + b;
end;


architecture behave of shiftLeft2 is
begin
  y <= a(29 downto 0) & "00";
end;


architecture behave of signext is
begin
  y <= X"0000" & a when a(15) = '0' else X"ffff" & a; 
end;

architecture behave of zeroext is
begin
  y <= X"0000" & a;
end;

architecture asynchronous of flopr is
begin
  process(clk, reset) begin
    if reset = '1' then  q <= CONV_STD_LOGIC_VECTOR(0, width); 
    elsif rising_edge(clk) then
      q <= d;
    end if;
  end process;
end;


architecture behave of mux2 is
begin
  y <= d0 when s = '0' else d1;
end;

architecture behave of mux3 is
begin
  y <= d0 when s="00" else
      d1 when s="01" else d2;
end;

architecture test of top is
  component mips 
    port(clk, reset:        in  STD_LOGIC;
         PC:                inout STD_LOGIC_VECTOR(31 downto 0);
         Instr:             in  STD_LOGIC_VECTOR(31 downto 0); 
         MemWrite:          out STD_LOGIC;
         ALUResult, WriteData: inout STD_LOGIC_VECTOR(31 downto 0);
         ReadData:          in  STD_LOGIC_VECTOR(31 downto 0));
  end component;
  
  component imem
    port(a:  in  STD_LOGIC_VECTOR(5 downto 0);
         rd: out STD_LOGIC_VECTOR(31 downto 0));
  end component;
  
  component dmem
    port(clk, we:  in STD_LOGIC;
         a, wd:    in STD_LOGIC_VECTOR(31 downto 0);
         rd:       out STD_LOGIC_VECTOR(31 downto 0));
  end component;
  
  signal PC, instrLocal, ALUOut, 
         ReadData: STD_LOGIC_VECTOR(31 downto 0);
begin
  -- instantiate processor and memories
  mips1: mips port map( clk 	  => clk, 
						reset 	  => reset, 
						PC        => PC, 
						Instr     => instrLocal, 
						MemWrite  => MemWrite, 
						ALUResult => ALUOut, 
                        WriteData => WriteData, 
						ReadData  => ReadData
					  );
					   
  imem1: imem port map( a	=> PC(7 downto 2), 
						rd	=> instrLocal
					  );
  
  dmem1: dmem port map( clk => clk, 
						we  => MemWrite, 
						a   => ALUOut, 
						wd  => WriteData, 
						rd  => ReadData
					  );
					  
  DataAdr <= ALUOut;
  
end;


architecture behave of dmem is
begin
  process is
    type ramtype is array (63 downto 0) of STD_LOGIC_VECTOR(31 downto 0);
    variable mem: ramtype;
  begin

    -- initialize memory
    for i in 0 to 63 loop
        mem(i) := X"ECE28100";
    end loop;
  
    loop 
      if rising_edge(clk) then
				
          if (we = '1') then mem(to_integer(unsigned(a(7 downto 2)))) := wd;
          end if;
      end if;
      rd <= mem(to_integer(unsigned(a(7 downto 2)))); 

      wait on clk, a;
    end loop;

  end process;
end;


architecture behave of imem is
begin
    process is

		 -- You'll need to create a path to your own file here.
		 file mem_file: text open read_mode is "C:\Users\C20Allison.Wong\Documents\USAFA\C3C_2nd_Semester\ECE281\ece281_wong_allison\Lab4\code\memfile2.dat";

		  
        variable L: line;
        variable ch: character;
        variable index, result: integer;
        type ramtype is array (63 downto 0) of STD_LOGIC_VECTOR(31 downto 0);
        variable mem: ramtype;
    begin
        -- initialize memory from file
        for i in 0 to 63 loop -- set all contents low
            mem(conv_integer(i)) := X"00000000";
        end loop;

        index := 0;
        while not endfile(mem_file) loop
            readline(mem_file, L);
           
                result := 0;
                    for k in 1 to 8 loop
                        read(L, ch);
                        if '0' <= ch and ch <= '9' then 
                            result := result*16 + character'pos(ch)-character'pos('0');
									
                        elsif 'a' <= ch and ch <= 'f' then
                            result := result*16 + character'pos(ch)-character'pos('a')+10;
									
                        else report "Format error on line " & integer'image(index)
                             severity error;
                        end if;
                    end loop;
        
            mem(index) := conv_std_logic_vector(result, 32);  
            index := index + 1;
        end loop;
        
        loop
            rd <= mem(conv_integer(a));
            wait on a;
        end loop;
		
    end process;
end;

