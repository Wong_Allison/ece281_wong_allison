--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : MooreElevatorController.vhd
--| AUTHOR(S)     : C3C Allison Wong
--| CREATED       : 03/13/2018
--| DESCRIPTION   : This file implements the CE4/Lab3 elevator controller (Moore Machine)
--|
--|  The system is specified as follows:
--|   - The elevator controller will traverse four floors (numbered 1 to 4).
--|   - It has two external inputs, Up_Down and Stop.
--|   - When Up_Down is active and Stop is inactive, the elevator will move up 
--|			until it reaches the top floor (one floor per clock, of course).
--|   - When Up_Down is inactive and Stop is inactive, the elevator will move down 
--|			until it reaches the bottom floor (one floor per clock).
--|   - When Stop is active, the system stops at the current floor.  
--|   - When the elevator is at the top floor, it will stay there until Up_Down 
--|			goes inactive while Stop is inactive.  Likewise, it will remain at the bottom 
--|			until told to go up and Stop is inactive.  
--|   - The system should output the floor it is on (1 – 4) as a four-bit binary number.
--|  
--| DOCUMENTATION : None.
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : None
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MooreElevatorController is
    Port ( clk     : in  STD_LOGIC;
           reset   : in  STD_LOGIC;
           stop    : in  STD_LOGIC;
           up_down : in  STD_LOGIC;
           floor   : out STD_LOGIC_VECTOR (3 downto 0)		   
		 );
end MooreElevatorController;

-- Write the code in a similar style as the Lesson 19 ICE (stoplight FSM version 2)
architecture Behavioral of MooreElevatorController is

    -- Below you create a new variable type! You also define what values that 
    -- variable type can take on. Now you can assign a signal as 
    -- "sm_floor" the same way you'd assign a signal as std_logic
	type sm_floor is (s_floor1, s_floor2, s_floor3, s_floor4);
	
	-- Here you create variables that can take on the values
	-- defined above. Neat-o! super duper neat-o!
	signal current_state, next_state : sm_floor;

begin
	
	-- Next state logic ---------------------------------
	next_state_process : process (current_state, stop, up_down)
	begin
	   case current_state is       
	       when s_floor1 =>
--	           if ((up_down = '0' and stop = '0') or (stop = '1')) then
--	               next_state <= s_floor1;
	           if (up_down = '1' and stop = '0') then
	               next_state <= s_floor2;
	           else
	               next_state <= s_floor1;
	           end if;
	           
            when s_floor2 =>
                if (up_down = '0' and stop = '0') then
                    next_state <= s_floor1;
--                elsif (stop = '1') then
--                    next_state <= s_floor2;
                elsif (up_down = '1' and stop = '0') then
                    next_state <= s_floor3;
                else 
                    next_state <= s_floor2;
                end if;
                
            when s_floor3 =>
                if (up_down = '0' and stop = '0') then
                    next_state <= s_floor2;
--                elsif (stop = '1') then
--                    next_state <= s_floor3;
                elsif (up_down = '1' and stop = '0') then
                    next_state <= s_floor4; 
                else
                    next_state <= s_floor3;
                end if;    
           
            when s_floor4 =>
                if (up_down = '0' and stop = '0') then
                    next_state <= s_floor3;
--                elsif (stop = '1') then
--                    next_state <= s_floor4;
--                elsif (up_down = '1' and stop = '0') then
--                    next_state <= s_floor4;
                else
                    next_state <= s_floor4;
                end if;
        end case;
    end process;
	
	-- State memory w/ **synchronous** reset ------------
	
	-- reset is active high and will return elevator to floor1
    clk_process : process (clk, reset)
    begin
            if rising_edge(clk) then
                if reset = '1' then 
                    current_state <= s_floor1;
                else
                    current_state <= next_state;
                end if;
            end if;            
    end process;
	
	
	-- Output logic     ---------------------------------	
	
	-- default is floor1
	
	out_process : process(current_state)
	begin
	   case current_state is
	       
	       when s_floor1 =>
	           floor <= "0001";
	       when s_floor2 =>
	           floor <= "0010";
	       when s_floor3 =>
	           floor <= "0011";
	       when s_floor4 =>
	           floor <= "0100";
	       when others =>
	           floor <= "0001";
	   end case;
    end process;
	
--	floor <= "0001" when (current_state = s_floor1) else
--	                       "0010" when (current_state = s_floor2) else
--	                       "0100" when (current_state = s_floor3) else
--	                       "1000" when (current_state = s_floor4) else
--	                       "0001";

end Behavioral;

