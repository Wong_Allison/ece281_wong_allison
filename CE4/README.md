----
ECE 281 CE4 - Advanced Elevator Controller 
----

By C3C Allison Wong

Objectives
==========
- Create and simulate a synchronous sequential logic system, with the provided state diagram
- Write a testbench for a synchronous system to fully simulate the state machine's operation

Results
=======

Simulation Waveform
-------------------
****with s_floor1<= "0001", s_floor2<= "0010", s_floor3<= "0100", s_floor4<= "1000", to showing the change in floors clearly as "stairs" on the waveform ****
![](images/SimulationWaveform.JPG)

****with s_floor1<= "0001", s_floor2<= "0010", s_floor3<= "0011", s_floor4<= "0100", as specified in the given state diagram ****
![](images/SimulationWaveform2.JPG)

Summary 
-------
The waveform shows the elevator going up, stopping at each floor for 2 clock cycles,  finally stopping at the top floor without using the stop input. Then it returns down to floor 1 and stops there, without using the stop input. Success.

Questions
=========
Q1: How many registers/flip-flops do you expect your design to require?

A1: With four states, the design is expected to have 2 flip-flops.

Q2: A contractor suggests that a Mealy Machine would yield a better elevator controller design. Do you agree? Why or why not? Think carefully about the implications. Discuss any advantages or disadvantages. Would the Mealy Machine behave differently? If so, how? Which elevator would you rather ride?

A2: In a Mealy version of the elevator state machine, you would have to press a button every time you wanted to move a single floor, which would be advantageous if that's what you wanted. The elevator we all know and love operates like the Moore machine does.

Q3: If you wrote the Mealy Machine next state logic as a process, what signal(s) would need to be included in the sensitivity list?

A3: You would need the current state, stop, as well as up_down in the sensitivity list.

Feedback
========

Number of hours spent on CE4 : 2.5 hours

Suggestions to improve CE4 in future years: None.

Documentation Statement
=======================
None.