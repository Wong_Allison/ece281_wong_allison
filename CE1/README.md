ECE 281 CE1 – Git it done with Markdown

![](media/065603d012e39dcd9c4c6fc3a25be1b0.jpg)

Number of hours spent on CE1: 1

Suggestions to improve CE1 in future years: Have instructions for those who have
conflicting software already installed from previous CompSci classes.
