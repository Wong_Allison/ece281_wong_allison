----
ECE 281 Lab2 - Thunderbird Lights
----

By C3C Allison Wong

Objectives
==========

-   Design, write, test, and implement in hardware a finite state machine to simulate the taillights of a 1965 Ford Thunderbrd.

Preliminary Design
==================

State Transition Diagram
------------------------
![](images/StateTransitionDiagramShot.jpg)

- The finite state machine will be a Moore machine.

Encoding Table
--------------
![](images/EncodingTableFixed.JPG)

Next State Table
----------------
![](images/NextStateTable.JPG)

Output Table
------------
![](images/OutputTable.JPG)

**FINAL EQUATIONS BOLDED**

Next State Equations
--------------------
**Q2*** = (OFF)(Ls) + L1 + L2 = (Q2')(Q1')(Q0')(Ls) + (Q2)(Q1')(Q0') + (Q2)(Q1')(Q0) = **(Q2')(Q1')(Q0')(Ls) + (Q2)(Q1')**

**Q1*** = (OFF)(Ls)(Rs) + R1 + R2 + L2 = (Q2')(Q1')(Q0')(Ls)(Rs) + (Q2')(Q1')(Q0) + (Q2')(Q1)(Q0') + (Q2)(Q1')(Q0) = **(Q2')(Q1')(Q0')(Ls)(Rs) + (Q2')(Q1)(Q0') + (Q1')(Q0)**

**Q0*** = (OFF)(Rs) + R2 + L1 = **(Q2')(Q1')(Q0')(Rs) + (Q2')(Q1)(Q0') + (Q2)(Q1')(Q0')**

Output Equations
----------------
**La** = L1 + L2 + L3 + ON = (Q2)(Q1')(Q0') + (Q2)(Q1')(Q0) + (Q2)(Q1)(Q0') + (Q2)(Q1)(Q0) = **Q2**

**Lb** = L2 + L3 + ON = (Q2)(Q1')(Q0) + (Q2)(Q1)(Q0') + (Q2)(Q1)(Q0) = **(Q2)(Q1')(Q0) + (Q2)(Q1)**

**Lc** = L3 + ON = (Q2)(Q1)(Q0') + (Q2)(Q1)(Q0) = **(Q2)(Q1)**

**Ra** = R1 + R2 + R3 + ON = (Q2')(Q1')(Q0) + (Q2')(Q1)(Q0') + (Q2')(Q1)(Q0) + (Q2)(Q1)(Q0) = **(Q2')(Q1) + (Q2')(Q1')(Q0) + (Q2)(Q1)(Q0)**

**Rb** = R2 + R3 + ON = (Q2')(Q1)(Q0') + (Q2')(Q1)(Q0) + (Q2)(Q1)(Q0) = **(Q2')(Q1) + (Q2)(Q1)(Q0)**

**Rc** = R3 + ON = (Q2')(Q1)(Q0) + (Q2)(Q1)(Q0) = **(Q1)(Q0)**

Schematic
---------
![](images/SchematicShot.jpg)

Results
=======

Fixed Schematic Waveform
------------------------
![](images/SimulationWaveformFixed.JPG)

Schematic Waveform v2
---------------------
**...when there's another way.**
![](images/SimulationWaveform.JPG)

Top_Level RTL Schematic Draft
-----------------------------
![](images/RTLSchematicDraft.JPG)

Top-Level RTL Schematic
-----------------------
![](images/RTLSchematic.JPG)

RTL Component Report
--------------------
![](images/RTLComponentReportFix.JPG)

Utilization Summary
-------------------
![](images/UtilizationSummary.JPG)
![](images/UtilizationSummary2.JPG)

Summary
-------
The results were as expected, when I did it with both the binary encoding/equation method, and with the enumerated types. Both waveforms are shown above. Success.

Feedback
========

Number of hours spent on Lab2: 5 hours

Suggestions to improve Lab2 in future years: I liked this one. Maybe even more conceptual explanation in the beginning, making clear that the top level is where the two components are linked.

Documentation Statement
=======================

Consulted Capt Warner, on changing my thunderbird_fsm design from enum to binary encoded.
