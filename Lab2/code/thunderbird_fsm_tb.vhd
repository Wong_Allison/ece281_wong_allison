--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : thunderbird_fsm_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : C3C Allison Wong
--| CREATED       : 03/02/2017
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : None
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity thunderbird_fsm_tb is
end thunderbird_fsm_tb;

architecture test_bench of thunderbird_fsm_tb is 
	
  component thunderbird_fsm is
    port(
        i_left, i_right  : in std_logic; 
        i_clk, i_reset : in std_logic;
        o_lights_R  : out std_logic_vector(2 downto 0);  -- a = 0, b = 1, c = 2
        o_lights_L  : out std_logic_vector(2 downto 0) -- a = 0, b = 1, c = 2
    );	
  end component;
  
--inputs
signal c_clk : std_logic := '0';
signal c_reset : std_logic := '0';
signal c_left : std_logic := '0';
signal c_right : std_logic := '0';

--outputs  
signal c_lights_R : std_logic_vector(2 downto 0);
signal c_lights_L : std_logic_vector(2 downto 0);

--clock period constant
constant clk_per : time := 10 ns;

  
begin
    
  uut: thunderbird_fsm port map (
        i_clk => c_clk,
        i_reset => c_reset,
        i_left => c_left,
        i_right => c_right,
        o_lights_R => c_lights_R,
        o_lights_L => c_lights_L  
  );

clk : process
    begin
        c_clk <= '0';
        wait for 5 ns;
        c_clk <= '1';
        wait for 5ns;
    end process;
    
    
	-- Test Plan Process --------------------------------
	-- Implement the test plan here.  Body of process is continuously from time = 0  
	test_process : process 
	begin
		-- reset (state 0)
		c_reset <= '1'; c_left <= '0'; c_right <= '0';
		wait for clk_per;
		
		--signal left 
		c_reset <= '0'; c_left <= '1'; c_right <= '0';
		wait for clk_per*3;
		
        --signal right
		c_reset <= '0'; c_left <= '0'; c_right <= '1';
        wait for clk_per*3;
        
        --hazards
		c_reset <= '0'; c_left <= '1'; c_right <= '1';
        wait for clk_per*5;
        
        --reset during hazards
        c_reset <= '1'; c_left <= '1'; c_right <= '1';
        wait for clk_per*2;        
        
        --reset while left blinking
        c_reset <= '1'; c_left <= '1'; c_right <= '0';
        wait for clk_per*3;
        
		wait; -- wait forever
	end process;	
	-----------------------------------------------------	
end test_bench;
